import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import electron from 'vite-plugin-electron'
import electronRender from 'vite-plugin-electron-renderer'
import VitePluginElectronTry from './plugins/vite-plugin-electron-try'
import path from 'path'
import AutoImport from 'unplugin-auto-import/vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      imports: [
        {
          'axios': [['default', 'axios']]
        },
        {
          'electron': ['clipboard', 'contextBridge', 'crashReporter', 'ipcRenderer', 'nativeImage', 'webFrame']
        }
      ],
      dirs: ['./src/utils', './src/example/service/api'],
      dts: 'src/types/auto-import.d.ts',
    }),
    electron([
      {
        entry: 'electron/index.ts'
      },
      {
        entry: 'electron/preload.ts'
      }
    ]),
    // VitePluginElectronTry({
    //   name: 'login'
    // }),
    electronRender(),
  ],
  resolve: {
    alias: {
      'utils': path.resolve(__dirname, "src/utils"),
    }
  }
})
