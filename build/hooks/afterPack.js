const fs = require("fs");

exports.default = async function (context) {
  const localeDir = context.appOutDir + "/locales/";

  fs.readdir(localeDir, function (err, files) {
    const links = files ?? [];
    for (const file of links) {
      const match = file.match(/zh-CN\.pak/);
      // 只保留中文
      if (match === null) fs.unlinkSync(localeDir + file);
    }
  });
};
