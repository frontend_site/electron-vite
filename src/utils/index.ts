import electron from 'electron'

interface logs {
  (): void
}

logs.ipcRenderer = electron.ipcRenderer

export function logs () {
  console.log('This is a example')
}

export default function defineCall () {
  console.log(window)
}