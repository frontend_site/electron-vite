import { autoUpdater } from 'electron-updater'
import { BrowserWindow, app, ipcMain } from 'electron'

const uploadUrl = 'http://127.0.0.1:8080/electrons/'

function sendUpdateMessage (text: string, mainWin: BrowserWindow) {
  mainWin.webContents.send('update', text)
}

export function updateHandle (mainWin: BrowserWindow) {
  let message = {
    updateAva: '检测到新版本，正在下载……',
    checking: '正在检查更新……',
    error: '检查更新出错',
    updateNotAva: '现在使用的就是最新版本，不用更新'
  }

  // 也可以通过代码配置文件服务地址
  autoUpdater.setFeedURL({
    url: uploadUrl,
    provider: 'generic',
  })

  // 取消自动下载
  autoUpdater.autoDownload = true

  // 正在检查更新
  autoUpdater.on('error', function (_error) {
    sendUpdateMessage(message.error, mainWin)
  })

  autoUpdater.on('checking-for-update', function () {
    sendUpdateMessage(message.checking, mainWin)
  })

  // 有新的可用更新
  autoUpdater.on('update-available', function (_info) {
    sendUpdateMessage(message.updateAva, mainWin)
  })

  //  没有可用的更新，也就是当前是最新版本
  autoUpdater.on('update-not-available', function (_info) {
    sendUpdateMessage(message.updateNotAva, mainWin)
  })

  // 更新下载进度事件
  autoUpdater.on('download-progress', function (progressObj) {
    mainWin.webContents.send('downloadProgress', progressObj)
  })

  // 新安装包下载完成
  autoUpdater.on('update-downloaded', (e) => {
    ipcMain.on('isUpdateNow', (_e, _arg) => {
      autoUpdater.quitAndInstall()
    })

    mainWin.webContents.send('isUpdateNow')
  })

  ipcMain.on('downloadUpdate', () => {
    autoUpdater.downloadUpdate()
  })

  ipcMain.on('checkForUpdate', () => {
    if (mainWin) {
      autoUpdater.checkForUpdates()
      console.log('-- checkForUpdate --')
    }
  })

  if (app.isPackaged) {
    autoUpdater.checkForUpdates()
  }
}